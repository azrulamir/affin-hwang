'use strict'

const mongoose = require('mongoose')
const mongoosePaginate = require('mongoose-paginate')

const CustomerSchema = mongoose.Schema({
    _id: mongoose.Schema.Types.ObjectId,
    name: {
        type: String,
        required: [true, 'Name is required.']
    },
    email: {
        type: String,
        required: [true, 'Email is required.']
    },
    age: {
        type: Number,
        required: [true, 'Age is required.']
    },
    user: {
        type: Number,
        required: [true, 'User is required.']
    }
}, { 
    timestamps: true 
})

CustomerSchema.plugin(mongoosePaginate);

module.exports = mongoose.model('Customer', CustomerSchema)