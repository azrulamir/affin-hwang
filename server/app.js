'use strict'

require('dotenv').config()

const cors = require('cors')
const express = require('express')
const app = express()
const bodyParser = require("body-parser")
const mongoose = require('mongoose')
const authMiddlware = require('./middleware/auth')

mongoose.set('useCreateIndex', true);
mongoose.connect(process.env.MONGODB_CONNECT_PATH, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useFindAndModify: false
})

app.use(bodyParser.urlencoded({ extended: true }))
app.use(bodyParser.json())
app.use(cors())

app.use('/auth', require('./routes/auth'))
app.use('/user', authMiddlware, require('./routes/user'))
app.use('/customer', authMiddlware, require('./routes/customer'))

app.listen(process.env.PORT, () => {
    console.log('Server started at port ' + process.env.PORT)
})