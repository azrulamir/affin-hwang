# API Server

Node Express RestAPI service

## Authentication

JWT

## Databases
MySQL

MongoDB

## Installation

Use the package manager (npm or yarn) to install.

```bash
yarn install
```

## Env
Copy env.example to .env and assign accordingly
```
PORT=3000

#MYSQL
DB_HOST=
DB_NAME=
DB_USERNAME=
DB_PASSWORD=

#JWT
JWT_SECRET=ZGAMAyjNkr16JAfC # Sample secret token
JWT_TOKEN_EXPIRE=24h

#MONGODB
MONGODB_CONNECT_PATH="" # Can use localhost or mongodb atlas
```

## Migration

```bash
yarn sequelize-cli db:migrate
```

## Seed

```bash
yarn sequelize-cli db:seed:all
```
This will seed the app with a default user with credentials below.
```
name: Superadmin
email: superadmin@example.com
password: secret
```

## Run
```bash
yarn run dev #development

OR

yarn start #production
```

## Consuming API
JWT token are required in each request headers apart from login route. Fire a POST request to the /auth/login route with the seeded credentials to get a JWT token.

```nodejs
axios
    .post("/auth/login", {
        email: 'superadmin@example.com',
        password: 'secret'
    })
    .then(resp => {
        console.log(resp.data.token)
    })
    .catch(err => {
        // catch error
    })
```

## Note
This app is specifically created for the purpose of testing, please use with caution.

## License
[MIT](https://choosealicense.com/licenses/mit/)