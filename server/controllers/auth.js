'use strict'

const jwt = require('jsonwebtoken')
const User = require('../models/mysql').User
const bcrypt = require('bcrypt')

const AuthController = {

    login: function (req, res) {

        let { email, password } = req.body

        if (email && password) {

            User.findOne({
                where: { email: email }
            })
                .then(user => {
                    if (user &&
                        user.email === req.body.email &&
                        bcrypt.compareSync(password, user.password)) {

                        // FIXME: Probably sigining with publicKey would make more 
                        // sense in a production env, but for demo purposes, 
                        // 32bit random string should suffice
                        let token = jwt.sign({ email: req.body.email }, process.env.JWT_SECRET, { expiresIn: process.env.JWT_TOKEN_EXPIRE })

                        res.status(200).json({
                            success: true,
                            message: 'Authentication successful!',
                            token: token,
                            expire: process.env.JWT_TOKEN_EXPIRE
                        })
                        return
                    }

                    else {
                        res.status(401).json({
                            success: false,
                            message: 'Incorrect username or password'
                        });
                    }
                })
                .catch(err => console.log(err)
                )

        }

        else {

            res.status(403).json({
                success: false,
                message: 'Authentication failed! Please check the request'
            })

        }
    }

}

module.exports = AuthController
