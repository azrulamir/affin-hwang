'use strict'

const Joi = require('@hapi/joi')
const Customer = require('../models/mongodb/customer')
const mongoose = require('mongoose')

const CustomerController = {

    /**
     * Return all customers
     * 
     * @param {*} req 
     * @param {*} res 
     * 
     */
    index: function (req, res) {
        Customer.paginate({}, {
            page: parseInt(req.query.page),
            limit: 3 // TODO: Probably need to change this
        })
            .then(result => {
                res.status(200).json(result)
            })
            .catch(err => console.log(err))
    },

    /**
     * Create customer
     * 
     * @param {*} req 
     * @param {*} res 
     */
    create: function (req, res) {

        if (this.validateInput(req, res).error) {
            res.status(422).json({
                message: "Input validation failed!",
                errors: this.validateInput(req, res).error.details
            })
            return
        }

        else {
            Customer.findOne({ email: req.body.email })
                .exec()
                .then(doc => {
                    if (doc !== null) {
                        res.status(409).json({
                            message: "Customer email already exist!",
                            data: doc
                        })
                        return
                    }
                    else {
                        let customer = new Customer({
                            _id: new mongoose.Types.ObjectId(),
                            name: req.body.name,
                            email: req.body.email,
                            age: req.body.age,
                            user: req.body.user.id
                        })

                        customer.save().then(doc => {
                            res.status(200).json({
                                message: "Customer registered sucessfully!"
                            })
                        })
                            .catch(err => console.log(err))
                    }
                })
                .catch(err => console.log(err))
        }
    },

    /**
     * Display ONE customer detail
     * 
     * @param {*} req 
     * @param {*} res 
     */
    show: function (req, res) {
        Customer.findById(req.params.id)
            .exec()
            .then(doc => {
                if (doc !== null) {
                    res.status(200).json(doc)
                    return
                }

                res.status(400).json({
                    message: "Oops something went wrong."
                })
            })
            .catch(err => {
                res.status(404).json({
                    message: "User does not exist!"
                })
            })
    },


    /**
     * Update ONE customer details
     * 
     * @param {*} req 
     * @param {*} res 
     * 
     * FIXME: Check for existing email
     */
    update: function (req, res) {

        if (this.validateInput(req, res).error) {
            res.status(422).json({
                message: "Input validation failed!",
                errors: this.validateInput(req, res).error.details
            })
            return
        }

        else {
            Customer.findByIdAndUpdate({ _id: req.params.id }, {
                $set: {
                    name: req.body.name,
                    email: req.body.email,
                    age: req.body.age
                }
            })
                .exec()
                .then(docs => {
                    res.status(200).json({
                        message: "Customer updated!"
                    })
                })
                .catch(err => console.log(err))
        }
    },

    /**
     * Delete customer
     * 
     * @param {*} req 
     * @param {*} res 
     */
    destroy: function (req, res) {
        Customer.deleteOne({ _id: req.params.id })
            .exec()
            .then(result => {
                if (result.deletedCount > 0) {
                    res.status(200).json({
                        message: "Customer deleted!"
                    })
                    return
                }
                else {
                    res.status(502).json({
                        message: "Oops, something went wrong!"
                    })
                }
            })
            .catch(err => console.log(err))
    },

    /**
     * Input validator
     * 
     * @param {*} req 
     * @param {*} res 
     */
    validateInput: function (req, res) {
        const schema = Joi.object({
            email: Joi.string().email({ minDomainSegments: 2, tlds: { allow: ['com', 'net', 'my'] } }),
            name: Joi.string().min(5).max(30).required(),
            age: Joi.number().required(),
            user: Joi.object().required()
        })
            .options({
                abortEarly: false,
                errors: {
                    escapeHtml: true
                }
            })

        return schema.validate(req.body)
    }

}

module.exports = CustomerController
