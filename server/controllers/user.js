'use strict'

const Joi = require('@hapi/joi')
const bcrypt = require('bcrypt')
const User = require('../models/mysql').User
const jwt = require('jsonwebtoken')

const UserController = {

    /**
     * Display all users
     * 
     * @param {*} req 
     * @param {*} res 
     */
    profile: function (req, res) {
        let authorization = req.headers.authorization
        let decoded = jwt.verify(authorization, process.env.JWT_SECRET)

        User.findOne({
            email: decoded.email
        }).then(user => {
            res.status(200).json({
                id: user.id,
                name: user.name,
                email: user.email,
                role: user.role
            })
        });
    },

    /**
     * Display all users
     * 
     * @param {*} req 
     * @param {*} res 
     */
    index: function (req, res) {
        User.findAll().then(users => {
            res.status(200).json(users)
        });
    },

    /**
     * Create ONE user
     * 
     * @param {*} req 
     * @param {*} res 
     */
    create: function (req, res) {

        const schema = Joi.object({
            email: Joi.string().email({ minDomainSegments: 2, tlds: { allow: ['com', 'net', 'my'] } }),
            name: Joi.string().min(5).max(30).required(),
            password: Joi.string().pattern(/^[a-zA-Z0-9]{3,30}$/).required(),
        })
        .options({
            abortEarly: false,
            errors: {
                escapeHtml: true
            }
        })

        let validation = schema.validate(req.body)

        if (validation.error) {
            res.send(validation.error.details)
            return
        }
        
        else {
            User.findOrCreate({
                where: { email: req.body.email }, 
                defaults: { 
                    name: req.body.name,
                    password: bcrypt.hashSync(req.body.password, 10)
                }
            })
                .then(([user, created]) => {
                    if (!created) {
                        let response = {
                            message: 'User already exist!',
                            data: user
                        }
                        res.send(response)
                        return
                    }
                    else {
                        let response = {
                            message: 'User created succesfully!',
                        }
                        res.send(response)
                    }
                })
        }
    }

}

module.exports = UserController
