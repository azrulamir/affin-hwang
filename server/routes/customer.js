'use strict'

const express = require('express')
const router = express.Router()

// Load controllers
const customer = require('../controllers/customer')

router.post('/', (req, res) => customer.create(req, res))
router.get('/', (req, res) => customer.index(req, res))
router.get('/:id', (req, res) => customer.show(req, res))
router.patch('/:id', (req, res) => customer.update(req, res))
router.delete('/:id', (req, res) => customer.destroy(req, res))

module.exports = router
