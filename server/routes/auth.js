'use strict'

const express = require('express')
const router = express.Router()

// Load controllers
const auth = require('../controllers/auth')

router.post('/login', (req, res, next) => auth.login(req, res, next))

module.exports = router
