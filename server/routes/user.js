'use strict'

const express = require('express')
const router = express.Router()

// Load controllers
const user = require('../controllers/user')

router.get('/', (req, res) => user.index(req, res))
router.post('/', (req, res) => user.create(req, res))
router.get('/profile', (req, res) => user.profile(req, res))

module.exports = router
