# SPA Client

VueJS SPA client that consume a restful API service


## Installation

Use the package manager (npm or yarn) to install.

```bash
yarn install
```

## Env
Assign .env accordingly
```
VUE_APP_NAME="SPA Demo" # default app name
VUE_APP_API_URL=http://... # REST API base url (with port if available)
```

## Run
```bash
yarn serve # development view

OR

yarn build # production build will be located in /dist folder
```

## Note
This app is specifically created for the purpose of testing, please use with caution.

## Improvements
```
Cache customer data locally around Vuex.
```

## License
[MIT](https://choosealicense.com/licenses/mit/)