import axios from 'axios';

/** Creating the instance for axios */
const httpClient = axios.create({
    baseURL: process.env.VUE_APP_API_URL
});

/** Adding the request interceptors */
httpClient.interceptors.request.use(
    request => {
        /** TODO: Add any request interceptors */
        return request;
    },
    error => {
        /** TODO: Do something with request error */
        return Promise.reject(error);
    }
)

/** Adding the response interceptors */
httpClient.interceptors.response.use(
    response => {
        /** TODO: Add any request interceptors */
        return response;
    },
    error => {
        /** TODO: Do something with response error */
        return Promise.reject(error);
    }
)

export { httpClient };