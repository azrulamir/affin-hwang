import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import BootstrapVue from 'bootstrap-vue'
import './sass/app.scss'
import { httpClient } from './services/httpClient'

Vue.use(BootstrapVue)

Vue.config.productionTip = false

// Reload token on refresh (if available)
const token = localStorage.getItem('session-token')
if (token) {
  httpClient.defaults.headers.common['Authorization'] = token
}

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
