import Vue from 'vue'
import VueRouter from 'vue-router'
import Login from '../views/Login.vue'
import CustomerList from '../views/Customer/List.vue'
import CustomerCreate from '../views/Customer/Create.vue'
import CustomerEdit from '../views/Customer/Edit.vue'
import store from '../store'

Vue.use(VueRouter)

const ifNotAuthenticated = (to, from, next) => {
  if (!store.getters.isAuthenticated) {
    next()
    return
  }
  next('/')
}

const ifAuthenticated = (to, from, next) => {
  if (store.getters.isAuthenticated) {
    next()
    return
  }
  next('/login')
}

const routes = [
  {
    path: '/',
    redirect: '/customer'
  },
  {
    path: '/login',
    name: 'login',
    component: Login,
    beforeEnter: ifNotAuthenticated
  },
  {
    path: '/customer',
    name: 'customer-list',
    component: CustomerList,
    beforeEnter: ifAuthenticated
  },
  {
    path: '/customer/create',
    name: 'customer-create',
    component: CustomerCreate,
    beforeEnter: ifAuthenticated
  },
  {
    path: '/customer/:id/edit',
    name: 'customer-edit',
    component: CustomerEdit,
    beforeEnter: ifAuthenticated
  }
]

const router = new VueRouter({
  routes
})

export default router
